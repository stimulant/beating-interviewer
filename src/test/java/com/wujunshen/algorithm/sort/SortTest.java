package com.wujunshen.algorithm.sort;

import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * 十大排序算法单元测试
 *
 * <p>1、冒泡排序 2、选择排序 3、插入排序 4、希尔排序 5、归并排序 6、快速排序 7、堆排序 8、计数排序 9、桶排序 10、基数排序
 *
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/20 00:04<br>
 */
@Slf4j
@DisplayName("排序测试")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SortTest {
  private static int[] sortArray = new int[5];

  @BeforeAll
  public static void init() {
    sortArray =
        new int[] {
          RandomUtils.nextInt(0, 1000),
          RandomUtils.nextInt(0, 1000),
          RandomUtils.nextInt(0, 1000),
          RandomUtils.nextInt(0, 1000),
          RandomUtils.nextInt(0, 1000)
        };
    log.info("生成的待排序整数数组为: {}", Arrays.toString(sortArray));
  }

  @AfterAll
  public static void cleanup() {
    sortArray = null;
  }

  @Order(1)
  @DisplayName("1.冒泡排序")
  @Test
  public void bubbleSort() {
    log.info(Arrays.toString(BubbleSort.bubbleSort(sortArray)));
  }

  @Order(2)
  @DisplayName("2.选择排序")
  @Test
  public void selectSort() {
    log.info(Arrays.toString(SelectSort.selectSort(sortArray)));
  }

  @Order(3)
  @DisplayName("3.插入排序")
  @Test
  public void insertSort() {
    log.info(Arrays.toString(InsertSort.insertSort(sortArray)));
  }

  @Order(4)
  @DisplayName("4.希尔排序")
  @Test
  void shellSort() {
    log.info(Arrays.toString(ShellSort.shellSort(sortArray)));
  }

  @Order(5)
  @DisplayName("5.归并排序")
  @Test
  public void mergeSort() {
    log.info(Arrays.toString(MergeSort.mergeSort(sortArray)));
  }

  @Order(6)
  @DisplayName("6.快速排序")
  @Test
  public void quickSort() {
    log.info(Arrays.toString(QuickSort.quickSort(sortArray)));
  }

  @Order(7)
  @DisplayName("7.堆排序")
  @Test
  public void heapSort() {
    log.info(Arrays.toString(HeapSort.heapSort(sortArray)));
  }

  @Order(8)
  @DisplayName("8.计数排序")
  @Test
  public void countSort() {
    log.info(Arrays.toString(CountSort.countSort(sortArray)));
  }

  @Order(9)
  @DisplayName("9.桶排序")
  @Test
  public void bucketSort() {
    log.info(Arrays.toString(BucketSort.bucketSort(sortArray)));
  }

  @Order(10)
  @DisplayName("10.基数排序")
  @Test
  public void radixSort() {
    log.info(Arrays.toString(RadixSort.radixSort(sortArray)));
  }
}
