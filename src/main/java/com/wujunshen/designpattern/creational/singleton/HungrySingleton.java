package com.wujunshen.designpattern.creational.singleton;

import java.io.Serializable;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/16 00:50<br>
 */
public class HungrySingleton implements Serializable {
  private static final HungrySingleton SINGLETON = new HungrySingleton();

  private HungrySingleton() {}

  /**
   * 通过该方法获取实例对象
   *
   * @return 实例对象
   */
  public static HungrySingleton getInstance() {
    return SINGLETON;
  }
}
