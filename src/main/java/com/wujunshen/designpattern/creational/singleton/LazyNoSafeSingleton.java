package com.wujunshen.designpattern.creational.singleton;

import java.io.Serializable;

/**
 * 懒汉式—线程不安全 <br>
 * 最基础的实现方式，线程上下文单例，不需要共享给所有线程，也不需要加synchronize之类的锁来提高性能
 *
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/13 04:08<br>
 */
public class LazyNoSafeSingleton implements Serializable {
  private static LazyNoSafeSingleton singleton = null;

  private LazyNoSafeSingleton() {}

  public static LazyNoSafeSingleton getInstance() {
    if (singleton == null) {
      // 线程会在这里等待
      singleton = new LazyNoSafeSingleton();
    }
    return singleton;
  }
}
