package com.wujunshen.designpattern.creational.singleton;

import java.io.Serializable;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/15 22:19<br>
 */
public class LazySafeSingleton1 implements Serializable {
  private static LazySafeSingleton1 singleton = null;

  private LazySafeSingleton1() {}

  public static synchronized LazySafeSingleton1 getInstance() {
    if (singleton == null) {
      singleton = new LazySafeSingleton1();
    }
    return singleton;
  }
}
