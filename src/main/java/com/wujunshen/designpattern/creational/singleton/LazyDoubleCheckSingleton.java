package com.wujunshen.designpattern.creational.singleton;

import java.io.Serializable;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/16 01:26<br>
 */
public class LazyDoubleCheckSingleton implements Serializable {
  /** 加volatile禁止指令重排序 */
  private static volatile LazyDoubleCheckSingleton singleton = null;

  private LazyDoubleCheckSingleton() {}

  public static LazyDoubleCheckSingleton getInstance() {
    // 检查是否阻塞
    if (singleton == null) {
      synchronized (LazyDoubleCheckSingleton.class) {
        // 检查是否要重新创建
        if (singleton == null) {
          singleton = new LazyDoubleCheckSingleton();
        }
      }
    }
    return singleton;
  }
}
