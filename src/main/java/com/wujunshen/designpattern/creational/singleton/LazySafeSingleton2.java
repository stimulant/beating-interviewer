package com.wujunshen.designpattern.creational.singleton;

import java.io.Serializable;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/15 22:38<br>
 */
public class LazySafeSingleton2 implements Serializable {
  private static LazySafeSingleton2 singleton = null;

  private LazySafeSingleton2() {}

  public static LazySafeSingleton2 getInstance() {
    synchronized (LazySafeSingleton2.class) {
      if (singleton == null) {
        singleton = new LazySafeSingleton2();
      }
    }

    return singleton;
  }
}
