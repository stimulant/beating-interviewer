package com.wujunshen.designpattern.creational.singleton;

import java.io.Serializable;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/16 03:16<br>
 */
public class Singleton implements Serializable {
  private Singleton() {}

  /**
   * 通过该方法获取实例对象
   *
   * @return 实例对象
   */
  public static Singleton getInstance() {
    return SingletonHolder.SINGLETON;
  }
  /** 静态成员内部类，该内部类的实例与外部类的实例没有绑定关系，而且只有被调用到才会装载，从而实现了延迟加载 */
  private static class SingletonHolder {
    private static final Singleton SINGLETON = new Singleton();
  }
}
