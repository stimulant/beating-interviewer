package com.wujunshen.designpattern.creational.singleton;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2021/2/16 04:39<br>
 */
public enum SingletonEnum {
  /** 实例 */
  INSTANCE;

  public static SingletonEnum getInstance() {
    return INSTANCE;
  }
}
