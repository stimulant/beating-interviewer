package com.wujunshen.thread.jmm;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/28 20:46<br>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Singleton {
  /** volatile关键字保证可见性 同时禁用指令重排（jdk1.5后生效） */
  private static volatile Singleton singleton;

  /**
   * 双重检查锁实现单例模式 推荐这样写，既保证线程安全，又延迟加载
   *
   * @return singleton instance
   */
  public static Singleton getSingleton() {
    if (singleton == null) {
      synchronized (Singleton.class) {
        if (singleton == null) {
          singleton = new Singleton();
        }
      }
    }
    return singleton;
  }
}
