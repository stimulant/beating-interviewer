package com.wujunshen.thread.lock.exception;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:44<br>
 */
public class TestThread implements Runnable {
  @Override
  public void run() {
    throw new RuntimeException("throwing runtimeException.....");
  }
}
