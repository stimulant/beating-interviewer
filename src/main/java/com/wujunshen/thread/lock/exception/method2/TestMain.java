package com.wujunshen.thread.lock.exception.method2;

import com.wujunshen.thread.lock.exception.TestThread;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:49<br>
 */
@Slf4j
public class TestMain {
  public static void main(String[] args) {
    try {
      TestThread t = new TestThread();
      ExecutorService exec = Executors.newCachedThreadPool(new HandlerThreadFactory());
      exec.execute(t);
    } catch (Exception e) {
      log.error(ExceptionUtils.getStackTrace(e));
    }
  }
}
