package com.wujunshen.thread.lock.exception.method2;

import java.util.concurrent.ThreadFactory;
import org.jetbrains.annotations.NotNull;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:47<br>
 */
public class HandlerThreadFactory implements ThreadFactory {
  @Override
  public Thread newThread(@NotNull Runnable runnable) {
    Thread t = new Thread(runnable);
    MyUncaughtExceptionHandler myUncaughtExceptionHandler = new MyUncaughtExceptionHandler();
    t.setUncaughtExceptionHandler(myUncaughtExceptionHandler);
    return t;
  }
}
