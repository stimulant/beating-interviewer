package com.wujunshen.thread.lock.exception.method1;

import com.wujunshen.thread.lock.exception.TestThread;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:45<br>
 */
@Slf4j
public class TestMain {
  public static void main(String[] args) {
    try {
      TestThread t = new TestThread();
      ExecutorService exec = Executors.newCachedThreadPool();
      Future<?> future = exec.submit(t);
      exec.shutdown();
      future.get(); // 主要是这句话起了作用，调用get()方法，异常重抛出，包装在ExecutorException
    } catch (Exception e) { // 这里可以把线程的异常继续抛出去
      log.error(ExceptionUtils.getStackTrace(e));
    }
  }
}
