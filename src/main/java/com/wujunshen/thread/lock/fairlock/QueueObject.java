package com.wujunshen.thread.lock.fairlock;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:41<br>
 */
public class QueueObject {
  private boolean isNotified = false;

  public synchronized void doWait() throws InterruptedException {
    while (!isNotified) {
      this.wait();
    }
    this.isNotified = false;
  }

  public synchronized void doNotify() {
    this.isNotified = true;
    this.notify();
  }

  @Override
  public boolean equals(Object o) {
    return this == o;
  }
}
