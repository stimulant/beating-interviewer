package com.wujunshen.thread.lock.deadlock.method1;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/16 03:41<br>
 */
@Slf4j
public class DeadLock implements Runnable {
  static final Object O_1 = new Object();
  static final Object O_2 = new Object();
  static int flag = 1;

  public static void main(String[] args) {
    DeadLock td1 = new DeadLock();
    DeadLock td2 = new DeadLock();
    td1.flag = 1;
    td2.flag = 0;
    Thread t1 = new Thread(td1);
    Thread t2 = new Thread(td2);
    t1.start();
    t2.start();
  }

  @Override
  public void run() {
    log.info("flag={}", flag);
    if (flag == 1) {
      synchronized (O_1) {
        try {
          Thread.sleep(500);
        } catch (Exception e) {
          log.error(ExceptionUtils.getStackTrace(e));
        }
        synchronized (O_2) {
          log.info("1");
        }
      }
    }

    if (flag == 0) {
      synchronized (O_2) {
        try {
          Thread.sleep(500);
        } catch (Exception e) {
          log.error(ExceptionUtils.getStackTrace(e));
        }
        synchronized (O_1) {
          log.info("0");
        }
      }
    }
  }
}
