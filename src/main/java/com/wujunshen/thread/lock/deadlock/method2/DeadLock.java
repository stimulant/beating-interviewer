package com.wujunshen.thread.lock.deadlock.method2;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:25<br>
 */
@Slf4j
public class DeadLock implements Runnable {
  public int flag = 1;

  public static void main(String[] args) {
    DeadLock td1 = new DeadLock();
    DeadLock td2 = new DeadLock();
    td1.flag = 1;
    td2.flag = 0;
    Thread t1 = new Thread(td1);
    Thread t2 = new Thread(td2);
    t1.start();
    t2.start();
  }

  @Override
  public synchronized void run() {
    log.info("flag={}", flag);
    if (flag == 1) {
      try {
        Thread.sleep(500);
      } catch (Exception e) {
        log.error(ExceptionUtils.getStackTrace(e));
      }

      log.info("1");
    }
    if (flag == 0) {
      try {
        Thread.sleep(500);
      } catch (Exception e) {
        log.error(ExceptionUtils.getStackTrace(e));
      }
      log.info("0");
    }
  }
}
