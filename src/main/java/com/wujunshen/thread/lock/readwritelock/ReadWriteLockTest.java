package com.wujunshen.thread.lock.readwritelock;

import java.util.Random;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/16 03:04<br>
 */
public class ReadWriteLockTest {
  public static void main(String[] args) {
    // 这是各线程的共享数据
    final TheData myData = new TheData();
    // 开启3个读线程
    for (int i = 0; i < 3; i++) {
      new Thread(
              () -> {
                while (true) {
                  myData.get();
                }
              })
          .start();
    }

    // 开启3个写线程
    for (int i = 0; i < 3; i++) {
      new Thread(
              () -> {
                while (true) {
                  myData.put(new Random().nextInt(10000));
                }
              })
          .start();
    }
  }
}
