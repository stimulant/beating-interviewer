package com.wujunshen.thread.producerandconsumer;

import com.wujunshen.thread.producerandconsumer.notify.Storage;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/16 03:21<br>
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Producer extends Thread {
  /** 每次生产的产品数量 */
  private int num;

  /** 所在放置的仓库 */
  private Storage storage;

  /**
   * 构造函数，设置仓库
   *
   * @param storage 仓库
   */
  public Producer(Storage storage) {
    this.storage = storage;
  }

  /** 线程run函数 */
  @Override
  public void run() {
    produce(num);
  }

  /**
   * 调用仓库Storage的生产函数
   *
   * @param num 生产数量
   */
  public void produce(int num) {
    storage.produce(num);
  }
}
