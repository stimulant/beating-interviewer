package com.wujunshen.thread.producerandconsumer.await;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/16 03:34<br>
 */
@Data
@Slf4j
public class Storage {
  /** 仓库最大存储量 */
  private static final int MAX_SIZE = 100;
  /** 锁 */
  private final Lock lock = new ReentrantLock();
  /** 仓库满的条件变量 */
  private final Condition full = lock.newCondition();
  /** 仓库空的条件变量 */
  private final Condition empty = lock.newCondition();
  /** 仓库存储的载体 */
  private Queue<Object> list = new LinkedList<>();

  /**
   * 生产num个产品
   *
   * @param num 生产数量
   */
  public void produce(int num) {
    // 获得锁
    lock.lock();

    // 如果仓库剩余容量不足
    while (list.size() + num > MAX_SIZE) {
      log.info("【要生产的产品数量】: {}", num);
      log.info("【库存量】: {} 暂时不能执行生产任务!", list.size());

      try {
        // 由于条件不满足，生产阻塞
        full.await();
      } catch (InterruptedException e) {
        log.error(ExceptionUtils.getStackTrace(e));
      }
    }

    // 生产条件满足情况下，生产num个产品
    for (int i = 1; i <= num; ++i) {
      list.add(new Object());
    }

    log.info("【已经生产产品数】: {}", num);
    log.info("【现仓储量为】: {}", list.size());
    // 唤醒其他所有线程
    full.signalAll();
    empty.signalAll();

    // 释放锁
    lock.unlock();
  }

  /**
   * 消费num个产品
   *
   * @param num 消费数量
   */
  public void consume(int num) {
    // 获得锁
    lock.lock();

    // 如果仓库存储量不足
    while (list.size() < num) {
      log.info("【要消费的产品数量】: {}", num);
      log.info("【库存量】: {} 暂时不能执行生产任务!", list.size());
      try {
        // 由于条件不满足，消费阻塞
        empty.await();
      } catch (InterruptedException e) {
        log.error(ExceptionUtils.getStackTrace(e));
      }
    }

    // 消费条件满足情况下，消费num个产品
    for (int i = 1; i <= num; ++i) {
      list.remove();
    }

    log.info("【已经消费产品数】: {}", num);
    log.info("【现仓储)量为】: {}", list.size());
    // 唤醒其他所有线程
    full.signalAll();
    empty.signalAll();

    // 释放锁
    lock.unlock();
  }
}
