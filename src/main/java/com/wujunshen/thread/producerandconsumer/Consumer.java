package com.wujunshen.thread.producerandconsumer;

import com.wujunshen.thread.producerandconsumer.notify.Storage;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/16 03:22<br>
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Consumer extends Thread {
  /** 每次消费的产品数量 */
  private int num;

  /** 所在放置的仓库 */
  private Storage storage;

  /**
   * 构造函数，设置仓库
   *
   * @param storage 仓库
   */
  public Consumer(Storage storage) {
    this.storage = storage;
  }

  /** 线程run函数 */
  @Override
  public void run() {
    consume(num);
  }

  /**
   * 调用仓库Storage的生产函数
   *
   * @param num 消费数量
   */
  public void consume(int num) {
    storage.consume(num);
  }
}
