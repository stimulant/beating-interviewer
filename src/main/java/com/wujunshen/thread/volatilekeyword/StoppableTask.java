package com.wujunshen.thread.volatilekeyword;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/18 11:38<br>
 */
public class StoppableTask extends Thread {
  int i1;
  volatile int i2;
  int i3;
  private volatile boolean pleaseStop;

  int geti1() {
    return i1;
  }

  int geti2() {
    return i2;
  }

  synchronized int geti3() {
    return i3;
  }

  @Override
  public void run() {
    while (!pleaseStop) {
      // do some stuff...
    }
  }

  public void tellMeToStop() {
    pleaseStop = true;
  }
}
