package com.wujunshen.algorithm.search;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/7/19 00:15<br>
 */
public class SequenceSearch {
  public int sequenceSearch(int[] a, int x) {
    for (int i = 0; i < a.length; i++) {
      if (x == a[i]) {
        return i;
      }
    }

    return -1;
  }
}
